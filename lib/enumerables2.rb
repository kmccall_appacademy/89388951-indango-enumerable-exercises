require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc += el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each {|sub_string| return false if !sub_string.include?(substring)}
  true
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.delete(" ").chars.select {|char| char if string.count(char) > 1}.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split(" ").sort_by {|word| word.length}.reverse[0..1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").select {|char| char if !string.include?(char)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| year if not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.chars == year.to_s.chars.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  repeating_songs = []
  songs.each_with_index {|song, i| repeating_songs << song if songs[i] == songs[i+1]}
  songs.select {|song| song if !repeating_songs.include?(song)}.uniq
end

def no_repeats?(song_name, songs)

end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  current_dist = string.length
  the_word = ""
  string.delete("!?.,").split.select {|word| current_dist, the_word = c_distance(word), word if c_distance(word) < current_dist}
  the_word
end

def c_distance(word)
  count = 0
  word.chars.reverse.each do |char|
      if char == "c"
        return count
      else
        count += 1
      end
    end
    count
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  answer = []
  sub_arr = []
  arr.each_with_index do |num, idx|
    if num == arr[idx + 1]
      sub_arr << idx
    elsif sub_arr.length >= 1
      sub_arr << idx
      answer << [sub_arr[0], sub_arr[-1]]
      sub_arr = []
    end
  end
  answer
end
